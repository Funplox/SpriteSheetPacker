package;

import app.AppMain;
import openfl.Lib;
import openfl.display.StageAlign;
import openfl.display.StageScaleMode;

/**
 * ...
 * @author Creative Magic
 */
class Main
{
	private var appInstance:AppMain;

	public function new() 
	{	
		setupStage();
		startup();
	}
	
	function setupStage():Void
	{
		Lib.current.stage.align = StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = StageScaleMode.NO_SCALE;
	}
	
	function startup():Void
	{
		appInstance = new AppMain();
	}
	
}