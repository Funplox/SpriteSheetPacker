package app;
import app.view.screens.ScreenList;
import com.creativemage.screenManager.ScreenManager;
import openfl.Lib;

/**
 * ...
 * @author Creative Magic
 */
class AppMain
{
	private var screenManager:ScreenManager;

	public function new() 
	{
		setScreenManager();
		
		init();
	}
	
	function setScreenManager():Void 
	{
		var portWidth = Lib.current.stage.stageWidth;
		var portHeight = Lib.current.stage.stageHeight;
		var screenList = new ScreenList();
		
		screenManager = new ScreenManager( Lib.current.stage, portWidth, portHeight, screenList );
	}
	
	function init():Void
	{
		screenManager.init(1);
	}
	
}