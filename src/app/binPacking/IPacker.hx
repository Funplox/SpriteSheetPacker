package app.binPacking;
import openfl.display.BitmapData;

/**
 * @author Creative Magic
 */
interface IPacker 
{
	public function pack(items:Array<BitmapData>, sheetWidth:Int, sheetHeight:Int):Void;
}