package app.binPacking.events;
import openfl.events.Event;

/**
 * ...
 * @author Creative Magic
 */
class PackerEvent extends Event
{
	public static inline var ITEM_NOT_FITTED:String = "itemNotFitted";
	static public inline var PACKING_COMPLETE:String = "packingComplete";

	public function new(type:String, bubbles:Bool = false, cancelable:Bool = false ) 
	{
		super(type, bubbles, cancelable );
	}
	
}