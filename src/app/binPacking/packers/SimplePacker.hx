package app.binPacking.packers;
import app.binPacking.IPacker;
import app.binPacking.events.PackerEvent;
import app.binPacking.packers.SimplePacker.Slot;
import openfl.display.BitmapData;
import openfl.events.EventDispatcher;
import openfl.geom.Point;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Creative Magic
 */
class SimplePacker extends EventDispatcher implements IPacker
{
	public var canvas:BitmapData;
	
	public var slots:Array<Slot> = [];

	public function new() 
	{
		super();
	}
	
	/* INTERFACE app.binPacking.IPacker */
	
	public function pack(items:Array<BitmapData>, sheetWidth:Int, sheetHeight:Int) 
	{
		items.sort( sortByHeight );
		
		canvas = new BitmapData( sheetWidth, sheetHeight, true, 0x00000000 );
		
		var w:Int = 0;
		var _y:Int = 0;
		var largestItemHeight = 0;
		
		for ( item in items )
		{
			if ( item.width + w > canvas.width )
			{
				_y += largestItemHeight;
				w = 0;
				largestItemHeight = 0;
			}
			
			if ( _y + item.height > canvas.height )
			{
				dispatchEvent( new PackerEvent(PackerEvent.ITEM_NOT_FITTED ) );
				continue;
			}
			
			var slot = new Slot();
			slot.x = w;
			slot.y = _y;
			slot.width = item.width;
			slot.height = item.height;
			slot.takenBy = item;
			slots.push(slot);
			
			w += item.width;
			
			if ( item.height > largestItemHeight )
				largestItemHeight = item.height;
				
			
		}
		
		
		renderCanvas();
		dispatchEvent( new PackerEvent(PackerEvent.PACKING_COMPLETE ) );
	}
	
	
	
	function renderCanvas():Void
	{
		for ( slot in slots )
		{
			if (slot.takenBy == null )
				continue;
			canvas.copyPixels( slot.takenBy, slot.takenBy.rect, new Point( slot.x, slot.y), null, null, true );
		}
	}
	
	function sortByHeight(a:BitmapData, b:BitmapData):Int 
	{
		if ( a.height > b.height )
			return -1;
		if ( b.height > a.height )
			return 1;
		
		return 0;
	}
}


class Slot extends Rectangle
{
	public var takenBy:BitmapData = null;
	
	public function new()
	{
		super();
	}
	
	override public function toString():String 
	{
		var s = super.toString() + " isTaken=" + (takenBy != null);
		return s;
	}
}