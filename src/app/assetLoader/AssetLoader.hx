package app.assetLoader;
import app.assetLoader.events.AssetLoaderEvent;
import app.data.AssetData;
import openfl.display.BitmapData;
import openfl.display.Loader;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import openfl.net.URLRequest;

/**
 * ...
 * @author Creative Magic
 */
class AssetLoader extends EventDispatcher
{
	public var loadedItems(default, null):Array<AssetData> = [];
	private var queuedItems:Array<String> = [];
	private var loadedItemCount:Int = 0;
	
	private var loader:Loader;
	private var urlRequest:URLRequest;

	public function new() 
	{
		super();
		
		loader = new Loader();
		loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onItemLoaded );
		
		urlRequest = new URLRequest();
	}
	
	public function addItemtoQueue(itemURL:String):Void
	{
		queuedItems.push(itemURL);
	}
	
	public function loadItems():Void
	{
		loadedItemCount = 0;
		loadItemByIndex( loadedItemCount);
	}
	
	private function loadItemByIndex(index:Int)
	{
		urlRequest.url = queuedItems[index];
		loader.load( urlRequest );
	}
	
	// EVENT HANDLERS
	
	private function onItemLoaded(e:Event):Void 
	{
		var assetData = new AssetData();
		assetData.assetOriginalURL = queuedItems[loadedItemCount];
		
		var bd = new BitmapData( Std.int( loader.content.width ), Std.int( loader.content.height ), true, 0x00000000 );
		bd.draw( loader.content );
		
		assetData.bitmapData = bd;
		assetData.assetWidth = bd.width;
		assetData.assetHeight = bd.height;
		loadedItems.push(assetData);
		
		loadedItemCount++;
		
		if ( loadedItemCount >= queuedItems.length )
		{
			dispatchEvent( new AssetLoaderEvent(AssetLoaderEvent.ALL_ASSETS_LOADED) );
			return;
		}
		
		loadItemByIndex(loadedItemCount);
	}
	
}