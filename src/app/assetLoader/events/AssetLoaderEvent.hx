package app.assetLoader.events;
import openfl.events.Event;

/**
 * ...
 * @author Creative Magic
 */
class AssetLoaderEvent extends Event
{
	public static inline var ALL_ASSETS_LOADED:String = "allAssetsLoaded";

	public function new(type:String, bubbles:Bool = false, cancelable:Bool = false) 
	{
		super(type, bubbles, cancelable);
	}
	
}