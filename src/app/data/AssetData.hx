package app.data;
import openfl.display.BitmapData;

/**
 * ...
 * @author Creative Magic
 */
class AssetData
{
	public var assetOriginalURL:String;
	
	public var assetWidth:Int;
	public var assetHeight:Int;
	
	public var assetX:Int;
	public var assetY:Int;
	
	public var bitmapData:BitmapData;

	public function new() 
	{
		
	}
	
}