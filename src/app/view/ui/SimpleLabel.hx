package app.view.ui;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFieldAutoSize;

/**
 * ...
 * @author Creative Magic
 */
class SimpleLabel extends Sprite
{
	public var text(get, set):String;
	public var size(get, set):Int;
	
	private var textField:TextField;

	public function new() 
	{
		super();
		
		textField = new TextField();
		textField.autoSize = TextFieldAutoSize.LEFT;
		textField.multiline = false;
		textField.wordWrap = false;
		textField.selectable = false;
		addChild(textField);
	}
	
	// GETTERS AND SETTERS
	
	private function set_text(value:String):String
	{
		return textField.text = value;
	}
	
	private function get_text():String
	{
		return textField.text;
	}
	
	private function set_size(value:Int):Int
	{
		var tf = textField.getTextFormat().clone();
		tf.size = value;
		
		textField.setTextFormat( tf );
		
		return value;
	}
	
	private function get_size():Int
	{
		return textField.getTextFormat().size;
	}
}