package app.view.ui;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.MouseEvent;

/**
 * ...
 * @author Creative Magic
 */
class SimpleRadioButtonGroup extends Sprite
{
	public var selectedIndex(default, set):Int = 0;
	private var radioButtons:Array<RadioButton> = [];

	public function new() 
	{
		super();
	}
	
	public function addRadioButton(label:String):Int
	{
		var radioButton = new RadioButton();
		radioButton.text = label;
		radioButton.y = 23 * radioButtons.length;
		radioButton.addEventListener(MouseEvent.CLICK, onRadioButtonClick);
		addChild(radioButton);
		
		radioButton.index = radioButtons.push(radioButton) - 1;
		selectButtonByIndex( radioButton.index );
		
		
		return radioButton.index;
	}
	
	public function removeRadioButtons(id:Int):Void
	{
		
	}
	
	private function selectButtonByIndex(index:Int):Void
	{	
		for ( i in 0...radioButtons.length )
		{
			radioButtons[i].selected = false;
			
			if ( radioButtons[i].index == index )
				radioButtons[i].selected = true;
		}
		
		dispatchEvent(new Event(Event.CHANGE));
	}
	
	// EVENT HANDLERS
	
	private function onRadioButtonClick(e:MouseEvent):Void 
	{
		var radioButton = cast e.currentTarget;
		selectedIndex = radioButton.index;
	}
	
	// GETTERS AND SETTERS
	
	private function set_selectedIndex(value:Int):Int
	{
		selectedIndex = value;
		selectButtonByIndex(value);
		return selectedIndex = value;
	}
}

private class RadioButton extends Sprite
{
	
	public var text(get, set):String;
	public var selected(default, set):Bool;
	
	private var label:SimpleLabel;
	private var overlaySprite:Sprite;
	
	public var index:Int = 0;
	
	public function new()
	{
		super();
		
		setComponents();
	}
	
	function setComponents() 
	{
		graphics.beginFill(0x939393);
		graphics.drawCircle( 9, 9, 9 );
		graphics.endFill();
		
		label = new SimpleLabel();
		label.x = 23;
		addChild(label);
		
		overlaySprite = new Sprite();
		addChild(overlaySprite);
	}
	
	function drawTickSprite() 
	{
		graphics.clear();
		graphics.beginFill(0x939393);
		graphics.drawCircle( 9, 9, 9 );
		graphics.endFill();
		
		if (selected == true)
		{
			graphics.beginFill(0x3A3A3A);
			graphics.drawCircle( 9, 9, 4 );
			graphics.endFill();
		}
	}
	
	function updateOverlaySprite():Void
	{
		overlaySprite.graphics.clear();
		overlaySprite.graphics.beginFill(0xFF0000, 0);
		overlaySprite.graphics.drawRect(0, 0, this.width, this.height);
		overlaySprite.graphics.endFill();
		
	}
	
	// GETTERS AND SETTERS
	
	private function get_text():String
	{
		return label.text;
	}
	
	private function set_text(value:String):String
	{
		label.text = value;
		updateOverlaySprite();
		return label.text;
	}
	
	private function set_selected(value:Bool):Bool
	{
		selected = value;
		drawTickSprite();
		return selected;
	}
	
}