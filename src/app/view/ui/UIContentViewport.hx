package app.view.ui;

import motion.Actuate;
import openfl.display.DisplayObject;
import openfl.display.Shape;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.KeyboardEvent;
import openfl.events.MouseEvent;
import openfl.geom.Point;
import openfl.geom.Rectangle;
import openfl.Lib;
import openfl.ui.Keyboard;

/**
 * ...
 * @author Creative Magic
 */

enum ScrollDirection
{
	Vertical;
	Horizontal;
	FreeScroll;
}
 
class UIContentViewport extends Sprite
{
	public var scrollDirection:ScrollDirection;
	public var allowScrolling:Bool = true;
	
	public var portWidth(default, set):Float = 0;
	public var portHeight(default, set):Float = 0;
	
	private var color:Int;
	private var portAlpha:Float;
	
	private var body:Shape;
	
	private var content:DisplayObject;
	
	private var startMousePosition:Point;
	
	private var contentOffsetX:Float;
	private var contentOffsetY:Float;

	public function new(w:Float, h:Float, bgColor:Int = 0xFFFFFF, alpha:Float = 1) 
	{
		super();
		
		scrollDirection = ScrollDirection.Vertical;
		startMousePosition = new Point();
		
		color = bgColor;
		portAlpha = alpha;
		
		setBody();
		setMask();
		
		setBG();
		
		addListeners();
		
		portWidth = w;
		portHeight = h;
	}
	
	// PUBLIC METHODS
	public function setContent(targetContent:DisplayObject, offsetX:Float = 0, offsetY:Float = 0):Void
	{
		contentOffsetX = offsetX;
		contentOffsetY = offsetY;
		
		content = targetContent;
		content.x = offsetX;
		content.y = offsetY;
		content.addEventListener(Event.RESIZE, onResize);
		addChild(content);
	}
	
	public function slideBack() 
	{
		if (scrollDirection == ScrollDirection.Vertical)
		{
			if (content.height <= portHeight)
			{
				if (content.y != contentOffsetY)
					Actuate.tween(content, .3, { y: contentOffsetY } ).snapping(true);
				return;
			}
			
			if (content.y > contentOffsetY)
				Actuate.tween(content, .3, { y: contentOffsetY } ).snapping(true);
			
			else if (content.y < portHeight - content.height - contentOffsetY)
				Actuate.tween(content, .3, { y: portHeight - content.height -  contentOffsetY} ).snapping(true);
		}
		else if (scrollDirection == ScrollDirection.Horizontal)
		{
			if (content.width <= portWidth)
			{
				if (content.x != contentOffsetX)
					Actuate.tween(content, .3, { x: contentOffsetX } ).snapping(true);
				return;
			}
			
			if (content.x > contentOffsetX)
				Actuate.tween(content, .3, { x: contentOffsetX } ).snapping(true);
			
			else if (content.x < portWidth - content.width - contentOffsetX)
				Actuate.tween(content, .3, { x: portWidth - content.width -  contentOffsetX} ).snapping(true);
		}
	}
	
	// PRIVATE METHODS
	function addListeners() 
	{
		addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
		addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
	}
	
	function setMask() 
	{
		this.scrollRect = new Rectangle(0, 0, portWidth, portHeight);
	}
	
	function setBody() 
	{
		body = new Shape();
		addChild(body);
	}
	
	function setBG() 
	{
		body.graphics.clear();
		body.graphics.beginFill(color, portAlpha);
		body.graphics.drawRect(0, 0, portWidth, portHeight);
		body.graphics.endFill();
	}
	
	function pageDown():Void
	{
		if (scrollDirection == ScrollDirection.Vertical)
		{
			content.y -= portHeight - 10;
			slideBack();
		}
		else if (scrollDirection == ScrollDirection.Horizontal)
		{
			content.x -= portWidth - 10;
			slideBack();
		}
	}
	
	function pageUp():Void
	{
		if (scrollDirection == ScrollDirection.Vertical)
		{
			content.y += portHeight - 10;
			slideBack();
		}
		else if (scrollDirection == ScrollDirection.Horizontal)
		{
			content.x += portWidth - 10;
			slideBack();
		}
	}
	
	function pageEnd():Void
	{
		if (scrollDirection == ScrollDirection.Vertical)
			content.y = portHeight - content.height -  contentOffsetY;
		else if (scrollDirection == ScrollDirection.Horizontal)
			content.x = portWidth - content.width -  contentOffsetX;
	}
	
	function pageHome():Void
	{
		if (scrollDirection == ScrollDirection.Vertical)
			content.y = contentOffsetY;
		else if (scrollDirection == ScrollDirection.Horizontal)
			content.x = contentOffsetX;
	} 
	
	// EVENT HANDLERS
	
	private function onKeyDown(e:KeyboardEvent):Void 
	{
		switch (e.keyCode)
		{
			case Keyboard.PAGE_DOWN:
			{
				pageDown();
			}
			
			case Keyboard.PAGE_UP:
			{
				pageUp();
			}
			
			case Keyboard.END:
			{
				pageEnd();
			}
			
			case Keyboard.HOME:
			{
				pageHome();
			}
		}
	}
	
	private function onMouseWheel(e:MouseEvent):Void 
	{
		if (scrollDirection == ScrollDirection.Vertical)
		{
			var newY:Float = content.y + e.delta * 6;
			content.y = newY;
		}
		else
		{
			var newX:Float = content.x + e.delta * 6;
			content.x = newX;
		}
		
		slideBack();
	}
	
	private function onResize(e:Event):Void 
	{
		if (content.height > portHeight)
			content.y = portHeight - content.height - contentOffsetY;
	}
	
	private function onFrame(e:Event):Void 
	{
		if (scrollDirection == ScrollDirection.Vertical)
			content.y = startMousePosition.y - mouseY;
		else
			content.x = startMousePosition.x - mouseX;		
	}
	
	private function onMouseDown(e:Event):Void
	{
		if (content == null || allowScrolling == false)
			return;
		
		startMousePosition.x = content.x + mouseX;
		startMousePosition.y = content.y + mouseY;
		
		Lib.current.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		addEventListener(Event.ENTER_FRAME, onFrame);
	}
	
	private function onMouseUp(e:Event):Void
	{
		Lib.current.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		removeEventListener(Event.ENTER_FRAME, onFrame);
		
		slideBack();
	}
	
	// GETTERS AND SETTERS
	
	private function set_portWidth(value:Float):Float
	{
		portWidth = value;
		setBG();
		setMask();
		return portWidth;
	}
	
	private function set_portHeight(value:Float):Float
	{
		portHeight = value;
		setBG();
		setMask();
		return portHeight;
	}
	
}