package app.view.ui;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.MouseEvent;

/**
 * ...
 * @author Creative Magic
 */
class SimpleButton extends Sprite
{
	var buttonLabel:app.view.ui.SimpleLabel;
	
	public var buttonWidth(default, set):Float = 0;
	public var buttonHeight(default, set):Float = 0;
	
	public var labelText(get, set):String;

	public function new() 
	{
		super();
		
		setupComponents();
		addEventListeners();
	}
	
	function addEventListeners() 
	{
		addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
		addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
	}
	
	function setupComponents() 
	{
		buttonLabel = new SimpleLabel();
		buttonLabel.mouseEnabled = false;
		buttonLabel.mouseChildren = false;
		addChild(buttonLabel);
	}
	
	private function updateGraphics():Void
	{
		graphics.clear();
		graphics.beginFill(0x939393);
		graphics.drawRect(0, 0, buttonWidth, buttonHeight);
		graphics.endFill();
		
		buttonLabel.x = (width - buttonLabel.width) * .5;
		buttonLabel.y = (height - buttonLabel.height) * .5;
	}
	
	// EVENT HANDLERS
	
	private function onMouseOver(e:MouseEvent):Void 
	{
		graphics.clear();
		graphics.beginFill(0xAAAAAA);
		graphics.drawRect(0, 0, buttonWidth, buttonHeight);
		graphics.endFill();
	}
	
	private function onMouseOut(e:MouseEvent):Void 
	{
		graphics.clear();
		graphics.beginFill(0x939393);
		graphics.drawRect(0, 0, buttonWidth, buttonHeight);
		graphics.endFill();
	}
	
	private function onMouseDown(e:MouseEvent):Void 
	{
		graphics.clear();
		graphics.beginFill(0x666666);
		graphics.drawRect(0, 0, buttonWidth, buttonHeight);
		graphics.endFill();
	}
	
	private function onMouseUp(e:MouseEvent):Void 
	{
		graphics.clear();
		graphics.beginFill(0xAAAAAA);
		graphics.drawRect(0, 0, buttonWidth, buttonHeight);
		graphics.endFill();
	}
	
	private function onRemovedFromStage(e:Event):Void 
	{
		removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
		removeEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
	}
	
	// GETTERS AND SETTERS
	
	private function set_buttonWidth(value:Float):Float
	{
		buttonWidth = value;
		updateGraphics();
		return buttonWidth;
	}
	
	private function set_buttonHeight(value:Float):Float
	{
		buttonHeight = value;
		updateGraphics();
		return buttonHeight;
	}
	
	private function get_labelText():String
	{
		return buttonLabel.text;
	}
	
	private function set_labelText(value:String):String
	{
		buttonLabel.text = value;
		
		if ( buttonWidth == 0 || buttonHeight == 0 )
		{
			graphics.clear();
			graphics.beginFill(0x939393);
			graphics.drawRect( 0, 0, buttonLabel.width + 10, buttonLabel.height + 10 );
			graphics.endFill();
			
			buttonLabel.x = buttonLabel.y = 5;
		}
		else
		{
			updateGraphics();
		}
		
		return buttonLabel.text;
	}
}