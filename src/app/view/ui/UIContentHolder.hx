package app.view.ui;

import openfl.display.DisplayObject;
import openfl.display.Sprite;
import openfl.events.Event;

using Lambda;

/**
 * ...
 * @author Creative Magic
 */

enum StorageOrder
{
	Consecutive;
	HorizontalLine;
	VerticalLine;
}
 
class UIContentHolder extends Sprite
{
	public var updateResize:Bool = false;

	private var contentStorageOrder:StorageOrder;
	private var contentMaxWidth:Int;
	private var contentMaxHeight:Int;
	
	public var contentHorizontalSpacing(default, null):Float;
	public var contentVerticalSpacing(default, null):Float;
	
	public var contentArray(default, null):Array<DisplayObject> = [];

	public function new(storageOrder:StorageOrder, maxWidth:Int = -1, maxHeight:Int = -1, horizontalSpacing:Float = 0, verticalSpacing:Float = 0) 
	{
		super();
		
		contentArray = new Array<DisplayObject>();
		
		contentStorageOrder = storageOrder;
		contentMaxWidth = maxWidth;
		contentMaxHeight = maxHeight;
		
		contentHorizontalSpacing = horizontalSpacing;
		contentVerticalSpacing = verticalSpacing;
	}
	
	public function clearAllContent():Void
	{
		for ( i in contentArray )
			i.x = i.y = 0;
		
		for (i in contentArray)
			removeChild(i);
		
		contentArray = [];
		
	}
	
	public function moveContentToTop(content:DisplayObject):Void
	{
		if ( contentArray.has ( content ) == true )
			addChild( content );
	}
	
	public function addContent(newContent:DisplayObject):Void
	{
		switch (contentStorageOrder)
		{
			case StorageOrder.Consecutive:
			{
				if (contentArray.length > 0)
				{
					
					var prevContent = contentArray[contentArray.length - 1];
					
					if (contentMaxWidth < 0)
					{
						newContent.x = prevContent.x + prevContent.width + contentHorizontalSpacing;
					}
					
					else if (prevContent.x + prevContent.width + contentHorizontalSpacing + newContent.width > contentMaxWidth)
					{
						newContent.x = 0;
						newContent.y = prevContent.y + prevContent.height + contentVerticalSpacing;
					}
					else
					{
						newContent.x = prevContent.x + prevContent.width + contentHorizontalSpacing;
						newContent.y = prevContent.y;
					}
				}
			}
			
			case StorageOrder.HorizontalLine:
			{
				if (contentArray.length > 0)
				{
					var prevContent = contentArray[contentArray.length - 1];
					
					newContent.x = prevContent.x + prevContent.width + contentHorizontalSpacing;
				}
			}
			
			case StorageOrder.VerticalLine:
			{
				if (contentArray.length > 0)
				{
					var prevContent = contentArray[contentArray.length - 1];
					newContent.y = prevContent.y + prevContent.height + contentVerticalSpacing;
				}
			}
			
		}
		
		contentArray.push(newContent);
		addChild(newContent);
		
		if (updateResize == true)
			dispatchEvent(new Event(Event.RESIZE));
	}
	
	public function removeContent(item:DisplayObject):Void 
	{
		removeChild( item );
		contentArray.remove( item );
		resetItemsOrder();
	}
	
	public function resetItemsOrder() 
	{
		for ( i in contentArray )
			i.x = i.y = 0;
		
		var tempArray = contentArray.slice( 0 );
		contentArray = [];
		
		for ( i in tempArray )
		{
			switch (contentStorageOrder)
			{
				case StorageOrder.Consecutive:
				{
					if (contentArray.length > 0)
					{
						var prevContent = contentArray[contentArray.length - 1];
						
						if (contentMaxWidth < 0)
						{
							i.x = prevContent.x + prevContent.width + contentHorizontalSpacing;
						}
						
						else if (prevContent.x + prevContent.width + contentHorizontalSpacing + i.width > contentMaxWidth)
						{
							i.x = 0;
							i.y = prevContent.y + prevContent.height + contentVerticalSpacing;
						}
						else
						{
							i.x = prevContent.x + prevContent.width + contentHorizontalSpacing;
							i.y = prevContent.y;
						}
					}
				}
				
				case StorageOrder.HorizontalLine:
				{
					if (contentArray.length > 0)
					{
						var prevContent = contentArray[contentArray.length - 1];
						
						i.x = prevContent.x + prevContent.width + contentHorizontalSpacing;
					}
				}
				
				case StorageOrder.VerticalLine:
				{
					if (contentArray.length > 0)
					{
						var prevContent = contentArray[contentArray.length - 1];
						i.y = prevContent.y + prevContent.height + contentVerticalSpacing;
					}
				}
				
			}
			
			contentArray.push(i);
		}
	}
	
	// TODO: need to add horizontal and consecutive cases
	public function reorderItemsByPositions(?order:StorageOrder):Void
	{
		if ( order == null )
			order = contentStorageOrder;
		
		switch ( order )
		{
			case StorageOrder.VerticalLine:
			{
				contentArray.sort( sortByY );
				
				contentArray[0].x = contentArray[0].y = 0;
				
				for ( i in 1...contentArray.length )
				{
					contentArray[i].x = 0;
					contentArray[i].y = contentArray[i -1].y + contentArray[i - 1].height + contentVerticalSpacing;
				}
			}
			
			default:
			{
				
			}
		}
			
		
		
	}
	
	public function putItemOnTop(item:DisplayObject) 
	{
		if ( contentArray.has( item ) == false )
			return;
			
		removeChild( item );
		addChild( item );
	}
	
	function sortByY(a:DisplayObject, b:DisplayObject):Int
	{
		if ( a.y == b.y )
			return 0;
		if ( a.y > b.y )
			return 1;
			
		return -1;
	}
	
	
	
}