package app.view.ui;
import openfl.display.Sprite;

/**
 * ...
 * @author Creative Magic
 */
class CheckboxWithInput extends Sprite
{
	var checkbox:app.view.ui.SimpleCheckbox;
	var input:app.view.ui.SimpleInput;
	
	public var checkboxLabelText(get, set):String;
	public var inputText(get, set):String;
	public var selected(get, set):Bool;

	public function new() 
	{
		super();
		
		setupComponents();
	}
	
	function setupComponents() 
	{
		checkbox = new SimpleCheckbox();
		checkbox.labelWidth = 80;
		checkbox.labelHeight = 18;
		addChild(checkbox);
		
		input = new SimpleInput();
		input.inputWidth = 100;
		input.inputHeight = 18;
		input.x = 100;
		addChild(input);
	}
	
	// GETTERS AND SETTERS
	
	private function get_checkboxLabelText():String
	{
		return checkbox.text;
	}
	
	private function set_checkboxLabelText(value:String):String
	{
		checkbox.text = value;
		return checkbox.text;
	}
	
	private function get_inputText():String
	{
		return input.text;
	}
	
	private function set_inputText(value:String):String
	{
		return input.text = value;
	}
	
	private function get_selected():Bool
	{
		return checkbox.selected;
	}
	
	private function set_selected(value:Bool):Bool
	{
		return checkbox.selected = value;
	}
}