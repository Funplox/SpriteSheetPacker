package app.view.ui;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFieldType;

/**
 * ...
 * @author Creative Magic
 */
class SimpleInput extends Sprite
{
	public var text(get, set):String;
	public var inputWidth(get, set):Float;
	public var inputHeight(get, set):Float;
	
	private var textField:TextField;

	public function new() 
	{
		super();
		
		textField = new TextField();
		textField.type = TextFieldType.INPUT;
		textField.background = true;
		textField.backgroundColor = 0xFFFFFF;
		textField.multiline = false;
		textField.wordWrap = false;
		addChild(textField);
	}
	
	// GETTERS AND SETTERS
	
	private function set_text(value:String):String
	{
		return textField.text = value;
	}
	
	private function get_text():String
	{
		return textField.text;
	}
	
	private function set_inputWidth(value:Float):Float
	{
		return textField.width = value;
	}
	
	private function get_inputWidth():Float
	{
		return textField.width;
	}
	
	private function set_inputHeight(value:Float):Float
	{
		return textField.height = value;
	}
	
	private function get_inputHeight():Float
	{
		return textField.height;
	}
	
}