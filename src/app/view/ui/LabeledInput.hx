package app.view.ui;
import openfl.display.Sprite;

/**
 * ...
 * @author Creative Magic
 */
class LabeledInput extends Sprite
{
	var label:app.view.ui.SimpleLabel;
	var input:app.view.ui.SimpleInput;
	public var labelText(get, set):String;
	public var inputText(get, set):String;

	public function new(_labelText:String = "", _inputText:String = "") 
	{
		super();
		
		setupComponents(_labelText, _inputText);
	}
	
	function setupComponents(_labelText:String, _inputText:String) 
	{
		label = new SimpleLabel();
		label.text = _labelText;
		addChild(label);
		
		
		input = new SimpleInput();
		input.text = _inputText;
		input.y = 25;
		input.inputWidth = 100;
		input.inputHeight = 22;
		addChild(input);
	}
	
	// GETTERS AND SETTERS
	
	private function get_labelText():String
	{
		return label.text;
	}
	
	private function set_labelText(value:String):String
	{
		return label.text = value;
	}
	
	private function get_inputText():String
	{
		return input.text;
	}
	
	private function set_inputText(value:String):String
	{
		return input.text = value;
	}
	
}