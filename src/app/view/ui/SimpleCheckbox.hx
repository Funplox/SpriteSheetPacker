package app.view.ui;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.MouseEvent;
import openfl.text.TextField;
import openfl.text.TextFieldAutoSize;

/**
 * ...
 * @author Creative Magic
 */
class SimpleCheckbox extends Sprite
{
	
	@:isVar public var selected(default, set):Bool = false;
	public var text(get, set):String;
	public var labelWidth(get, set):Float;
	public var labelHeight(get, set):Float;
	
	private var overlay:Sprite;
	private var checkSprite:Sprite;
	private var label:openfl.text.TextField;

	public function new() 
	{
		super();
		
		setComponents();
	}
	
	public function setLabelToAutoSize():Void
	{
		label.autoSize = TextFieldAutoSize.LEFT;
	}
	
	function setComponents() 
	{
		graphics.beginFill(0x939393);
		graphics.drawRect( 0, 0, 18, 18 );
		graphics.endFill();
		
		checkSprite = new Sprite();
		addChild(checkSprite);
		drawCheckSprite();
		
		label = new TextField();
		label.x = 25;
		label.autoSize = TextFieldAutoSize.LEFT;
		label.multiline = false;
		label.wordWrap = false;
		label.selectable = false;
		addChild(label);
		
		overlay = new Sprite();
		overlay.addEventListener(MouseEvent.CLICK, onOverlayClick);
		addChild(overlay);
		drawOverlaySprite();
		
	}
	
	function drawOverlaySprite() 
	{
		overlay.graphics.clear();
		overlay.graphics.beginFill( 0xFF0000, 0);
		overlay.graphics.drawRect( 0, 0, this.width, this.height );
		overlay.graphics.endFill();
	}
	
	private function drawCheckSprite():Void
	{
		checkSprite.graphics.clear();
		checkSprite.graphics.beginFill( 0x3A3A3A, (selected == true ) ? 1: 0);
		checkSprite.graphics.drawRect( 3, 3, 12, 12 );
		checkSprite.graphics.endFill();
	}
	
	// EVENT HANDLERS
	
	private function onOverlayClick(e:MouseEvent):Void 
	{
		selected = !selected;
	}
	
	// GETTERS AND SETTERS
	
	private function set_selected(value:Bool):Bool
	{
		selected = value;
		drawCheckSprite();
		dispatchEvent(new Event(Event.CHANGE, true));
		return selected;
	}
	
	private function set_text(value:String):String
	{
		label.text = value;
		drawOverlaySprite();
		return label.text;
	}
	
	private function get_text():String
	{
		return label.text;
	}
	
	private function get_labelWidth():Float
	{
		return label.width;
	}
	
	private function set_labelWidth(value:Float):Float
	{
		label.autoSize = TextFieldAutoSize.NONE;
		return label.width = value;
	}
	
	private function get_labelHeight():Float
	{
		return label.height;
	}
	
	private function set_labelHeight(value:Float):Float
	{
		label.autoSize = TextFieldAutoSize.NONE;
		return label.height = value;
	}
	
}