package app.view.ui;
import app.AppMain;
import openfl.display.Sprite;
import openfl.events.MouseEvent;

/**
 * ...
 * @author Creative Magic
 */
class SpecialItemInput extends Sprite
{
	public var itemNameText(get, set):String;
	public var gridSpawnInputText(get, set):String;
	public var shotSpawnInputText(get, set):String;
	
	public var itemEnableSelected(get, set):Bool;
	public var itemOnGridSelected(get, set):Bool;
	public var itemAsShotSelected(get, set):Bool;
	
	private var checkbox:app.view.ui.SimpleCheckbox;
	private var gridCheckbox:app.view.ui.SimpleCheckbox;
	private var gridChanceInput:app.view.ui.SimpleInput;
	private var shotCheckbox:app.view.ui.SimpleCheckbox;
	private var shotChanceInput:app.view.ui.SimpleInput;

	public function new() 
	{
		super();
		
		setupComponents();
	}
	
	private function setupComponents():Void
	{
		checkbox = new SimpleCheckbox();
		checkbox.addEventListener(MouseEvent.CLICK, onEnabledCheckboxClick);
		addChild(checkbox);
		
		gridCheckbox = new SimpleCheckbox();
		gridCheckbox.x = 70;
		addChild(gridCheckbox);
		
		gridChanceInput = new SimpleInput();
		gridChanceInput.x = 125;
		gridChanceInput.inputWidth = 37;
		gridChanceInput.inputHeight = 18;
		addChild(gridChanceInput);
		
		shotCheckbox = new SimpleCheckbox();
		shotCheckbox.x = 180;
		addChild(shotCheckbox);
		
		shotChanceInput = new SimpleInput();
		shotChanceInput.x = 235;
		shotChanceInput.inputWidth = 37;
		shotChanceInput.inputHeight = 18;
		addChild(shotChanceInput);
		
		setInputAvailability();
	}
	
	private function setInputAvailability():Void
	{
		if ( itemEnableSelected == false )
		{
			gridCheckbox.alpha = .2;
			gridCheckbox.mouseEnabled = false;
			gridCheckbox.mouseChildren = false;
			
			gridChanceInput.alpha = .2;
			gridChanceInput.mouseEnabled = false;
			gridChanceInput.mouseChildren = false;
			
			shotCheckbox.alpha = .2;
			shotCheckbox.mouseEnabled = false;
			shotCheckbox.mouseChildren = false;
			
			shotChanceInput.alpha = .2;
			shotChanceInput.mouseEnabled = false;
			shotChanceInput.mouseChildren = false;
		}
		else
		{
			gridCheckbox.alpha = 1;
			gridCheckbox.mouseEnabled = true;
			gridCheckbox.mouseChildren = true;
			
			gridChanceInput.alpha = 1;
			gridChanceInput.mouseEnabled = true;
			gridChanceInput.mouseChildren = true;
			
			shotCheckbox.alpha = 1;
			shotCheckbox.mouseEnabled = true;
			shotCheckbox.mouseChildren = true;
			
			shotChanceInput.alpha = 1;
			shotChanceInput.mouseEnabled = true;
			shotChanceInput.mouseChildren = true;
		}
	}
	
	// EVENT HANDLERS
	private function onEnabledCheckboxClick(e:MouseEvent):Void
	{
		set_itemEnableSelected(checkbox.selected);
	}
	
	// GETTERS AND SETTERS
	
	private function get_itemNameText():String
	{
		return checkbox.text;
	}
	
	private function set_itemNameText(value:String):String
	{
		return checkbox.text = value;
	}
	
	private function get_gridSpawnInputText():String
	{
		return gridChanceInput.text;
	}
	
	private function set_gridSpawnInputText(value:String):String
	{
		return gridChanceInput.text = value;
	}
	
	private function get_shotSpawnInputText():String
	{
		return shotChanceInput.text;
	}
	
	private function set_shotSpawnInputText(value:String):String
	{
		return shotChanceInput.text = value;
	}
	
	private function get_itemEnableSelected():Bool
	{
		return checkbox.selected;
	}
	
	private function set_itemEnableSelected(value:Bool):Bool
	{
		checkbox.selected = value;
		setInputAvailability();
		return checkbox.selected;
	}
	
	private function get_itemOnGridSelected():Bool
	{
		return gridCheckbox.selected;
	}
	
	private function set_itemOnGridSelected(value:Bool):Bool
	{
		return gridCheckbox.selected = value;
	}
	
	private function get_itemAsShotSelected():Bool
	{
		return shotCheckbox.selected;
	}
	
	private function set_itemAsShotSelected(value:Bool):Bool
	{
		return shotCheckbox.selected = value;
	}
	
}