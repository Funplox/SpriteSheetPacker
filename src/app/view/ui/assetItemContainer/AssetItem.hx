package app.view.ui.assetItemContainer;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFieldAutoSize;

/**
 * ...
 * @author Creative Magic
 */
class AssetItem extends Sprite
{
	private inline static var MAX_DISPLAYED_CHARS:Int = 30;
	
	public var url(default, set):String = "";
	
	private var nameTextField:TextField;
	public var closeButton(default, null):Sprite;

	public function new() 
	{
		super();
		
		setupTextField();
		setupCloseButton();
	}
	
	private function setupTextField():Void
	{
		nameTextField = new TextField();
		nameTextField.selectable = false;
		nameTextField.multiline = false;
		nameTextField.autoSize = TextFieldAutoSize.LEFT;
		addChild(nameTextField);
	}
	
	private function setupCloseButton():Void
	{
		closeButton = new Sprite();
		addChild(closeButton);
		
		var bitmap = new Bitmap( Assets.getBitmapData("img/closeButton.png" ) );
		closeButton.addChild(bitmap);
	}
	
	// GETTERS AND SETTERS
	
	private function set_url(value:String):String
	{
		url = value;
		
		if (value.length > MAX_DISPLAYED_CHARS )
			nameTextField.text = "..." + value.substring( value.length - MAX_DISPLAYED_CHARS , null );
		else
			nameTextField.text = url;
		
		return url;
	}
	
}