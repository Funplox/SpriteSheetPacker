package app.view.ui.assetItemContainer;

import app.view.ui.UIContentHolder;
import app.view.ui.UIContentViewport;
import openfl.display.Sprite;
import openfl.events.MouseEvent;

/**
 * ...
 * @author Creative Magic
 */
class AssetItemContainer extends Sprite
{
	public var portWidth(get, set):Float;
	public var portHeight(get, set):Float;
	
	private var contentViewport:UIContentViewport;
	private var contentHolder:UIContentHolder;
	
	public var items(default, null ):Array<AssetItem> = [];
	
	
	public function new() 
	{
		super();
		
		contentHolder = new UIContentHolder( StorageOrder.VerticalLine, -1, -1, 0, 0 );
		
		contentViewport = new UIContentViewport(0, 0, 0xAFAFAF, .3);
		contentViewport.allowScrolling = true;
		contentViewport.scrollDirection = ScrollDirection.Vertical;
		contentViewport.setContent( contentViewport );
		addChild(contentViewport);
	}
	
	public function addItem(url:String):Void
	{
		trace("adding item");
		var assetItem = new AssetItem();
		assetItem.closeButton.addEventListener(MouseEvent.CLICK, onItemRemoveButtonClick);
		assetItem.closeButton.x = portWidth - assetItem.closeButton.width - 10;
		assetItem.url = url;
		contentHolder.addContent(assetItem);
		
		items.push(assetItem);
	}
	
	// EVENT HANDLERS
	
	private function onItemRemoveButtonClick(e:MouseEvent):Void
	{
		var assetItem = cast e.currentTarget;
		
		items.remove(assetItem);
		removeChild(assetItem);
	}
	
	
	// GETTERS AND SETTERS
	
	private function get_portWidth():Float
	{
		return contentViewport.portWidth;
	}
	
	private function set_portWidth(value:Float):Float
	{
		return contentViewport.portWidth = value;
	}
	
	private function get_portHeight():Float
	{
		return contentViewport.portHeight;
	}
	
	private function set_portHeight(value:Float):Float
	{
		return contentViewport.portHeight = value;
	}
	
}