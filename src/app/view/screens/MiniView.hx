package app.view.screens;
import app.assetLoader.AssetLoader;
import app.assetLoader.events.AssetLoaderEvent;
import app.binPacking.events.PackerEvent;
import app.binPacking.packers.SimplePacker;
import app.view.ui.SimpleButton;
import app.view.ui.SimpleRadioButtonGroup;
import com.creativemage.screenManager.AScreen;
import flash.events.Event;
import haxe.Json;
import openfl.display.BitmapData;
import openfl.display.PNGEncoderOptions;
import openfl.events.MouseEvent;
import openfl.geom.Point;
import openfl.text.TextField;
import openfl.text.TextFieldType;
import openfl.utils.ByteArray;
import sys.io.File;
import sys.io.FileOutput;
import systools.Dialogs;

using Lambda;

/**
 * ...
 * @author Creative Magic
 */
class MiniView extends AScreen
{
	private var assetURLs:Array<String> = [];
	
	var imagesTextField:openfl.text.TextField;
	var assetLoader:app.assetLoader.AssetLoader;
	var sizeRadioGroup:app.view.ui.SimpleRadioButtonGroup;
	
	private var isPackable:Bool = true;
	private var packer:SimplePacker;

	public function new() 
	{
		super();
	}
	
	function setupUI() 
	{
		var loadButton = new SimpleButton();
		loadButton.labelText = "Load images";
		loadButton.buttonWidth = screenWidth * .9;
		loadButton.buttonHeight = 50;
		addScreenListener(loadButton, MouseEvent.CLICK, onLoadButtonClick);
		addChild(loadButton);
		
		var clearButton = new SimpleButton();
		clearButton.labelText = "Clear";
		clearButton.x = screenWidth * .9 + 1;
		clearButton.buttonWidth = screenWidth * .1 - 1;
		clearButton.buttonHeight = 50;
		addScreenListener(clearButton, MouseEvent.CLICK, onClearButtonClick);
		addChild(clearButton);
		
		imagesTextField = new TextField();
		imagesTextField.wordWrap = false;
		imagesTextField.type = TextFieldType.DYNAMIC;
		imagesTextField.y = loadButton.y + loadButton.height + 15;
		imagesTextField.width = screenWidth;
		imagesTextField.height = 450;
		imagesTextField.selectable = false;
		imagesTextField.background = true;
		imagesTextField.backgroundColor = 0xf1f1f1;
		addChild(imagesTextField);
		
		sizeRadioGroup = new SimpleRadioButtonGroup();
		sizeRadioGroup.addRadioButton("1024x1024px");
		sizeRadioGroup.addRadioButton("2048x2048px");
		sizeRadioGroup.addRadioButton("4096x4096px");
		sizeRadioGroup.selectedIndex = 0;
		sizeRadioGroup.x = 12;
		sizeRadioGroup.y = imagesTextField.y + imagesTextField.height + 15;
		addChild(sizeRadioGroup);
		
		var packButton = new SimpleButton();
		packButton.labelText = "Pack images";
		packButton.buttonWidth = screenWidth;
		packButton.buttonHeight = 50;
		packButton.y = screenHeight - packButton.height;
		addScreenListener( packButton, MouseEvent.CLICK, onPackButtonClick);
		addChild(packButton);
	}
	
	private function addURLs(urls:Array<String>):Void
	{
		for ( i in urls )
		{
			if ( assetURLs.has( i ) == true )
				continue;
			assetURLs.push(i);
		}
	}
	
	private function saveJSON(data:Array<Slot>, url:String):Void
	{
		
		
		var jsonData:String = Json.stringify( data );
		
		if ( url.indexOf(".json") == -1 )
			url = url + ".json";
			
		var fo:FileOutput = sys.io.File.write(url, false);
		fo.writeString(jsonData);
		fo.close();
	}
	
	private function saveImage(bitmapData:BitmapData, url:String):Void
	{
		var b:ByteArray = bitmapData.encode( bitmapData.rect, new PNGEncoderOptions() );
		
		if ( url.indexOf(".png") == -1 )
			url = url + ".png";
			
		var fo:FileOutput = sys.io.File.write(url, true);
		fo.writeString(b.toString());
		fo.close();
	}
	
	private function getSpriteSheetSize():Point
	{		
		return switch ( sizeRadioGroup.selectedIndex )
		{
			case 0:
				new Point( 1024, 1024 );
			case 1: 
				new Point( 2048, 2048 );
			case 2:
				new Point( 4096, 4096 );
			case 3:
				new Point( 8192, 8192 );
			default:
				new Point( 2048, 2048 );
		}
	}
	
	private function packImages():Void
	{
		
		var bitmaps:Array<BitmapData> = [];
		for ( item in assetLoader.loadedItems )
			bitmaps.push(item.bitmapData);
		
		var size = getSpriteSheetSize();
		packer.pack( bitmaps, Std.int(size.x), Std.int(size.y) );
		
	}
	
	function setupPacker() 
	{
		packer = new SimplePacker();
		addScreenListener( packer, PackerEvent.PACKING_COMPLETE, onPackingComplete);
	}
	
	// EVENT HANDLERS
	
	private function onItemNotFitted(e:Event):Void 
	{
		packer.removeEventListener( PackerEvent.ITEM_NOT_FITTED, onItemNotFitted);
		isPackable = Dialogs.confirm("Warning", "Some items could not fit the sprite sheet and will be omitted, continue anyway?", false );
	}
	
	private function onPackingComplete(e:Event):Void 
	{
		if ( isPackable == false )
			return;
		
		var url = Dialogs.saveFile( "Save sprite sheet", "", "", null);
		
		if ( url == null || url.length < 1 )
			return;
			
		saveImage( packer.canvas, url );
		saveJSON( packer.slots, url );
	}
	
	private function onClearButtonClick(e:Event):Void 
	{
		assetURLs = [];
		imagesTextField.text = "";
	}
	
	private function onAllAssetsLoaded(e:AssetLoaderEvent):Void 
	{
		packImages();
	}
	
	private function onLoadButtonClick(e:Event):Void 
	{
		var filters: FILEFILTERS = 
			{ count: 1
			, descriptions: ["Image files"]
			, extensions: ["*.jpg;*.jpeg;*.png;"]			
			};
		
		var selectedItems = Dialogs.openFile("Select image assets", "", filters);
		addURLs(selectedItems);
		
		var t = "";
		for ( url in assetURLs)
			t += url + "\n";
		imagesTextField.text = t;
	}
	
	private function onPackButtonClick(e:Event):Void 
	{
		isPackable = true;
		packer.addEventListener( PackerEvent.ITEM_NOT_FITTED, onItemNotFitted);
		
		// load all images into memory
		assetLoader = new AssetLoader();
		for (i in assetURLs)
			assetLoader.addItemtoQueue(i);
		assetLoader.addEventListener( AssetLoaderEvent.ALL_ASSETS_LOADED, onAllAssetsLoaded );
		assetLoader.loadItems();
	}
	
	override function onInit():Void 
	{
		setupUI();
		setupPacker();
	}
	
	
	
}