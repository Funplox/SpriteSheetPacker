package app.view.screens;
import app.binPacking.IPacker;
import app.binPacking.packers.SimplePacker;
import app.view.ui.SimpleButton;
import app.view.ui.SimpleLabel;
import app.view.ui.SimpleRadioButtonGroup;
import app.view.ui.UIContentHolder;
import app.view.ui.assetItemContainer.AssetItem;
import app.view.ui.assetItemContainer.AssetItemContainer;
import com.creativemage.screenManager.AScreen;
import flash.events.Event;
import openfl.Lib;
import openfl.events.MouseEvent;
import systools.Dialogs;

/**
 * ...
 * @author Creative Magic
 */
class MainScreen extends AScreen
{
	var assetsItemContainer:app.view.ui.assetItemContainer.AssetItemContainer;

	public function new() 
	{
		super();
	}
	
	private function setupUI():Void
	{
		
		//  Assets loading 
		var importLabel = new SimpleLabel();
		importLabel.text = "1. Import assets";
		importLabel.size = 18;
		importLabel.x = 8;
		importLabel.y = 16;
		addChild(importLabel);
		
		assetsItemContainer = new AssetItemContainer();
		assetsItemContainer.portWidth = 265;
		assetsItemContainer.portHeight = 630;
		assetsItemContainer.x = 10;
		assetsItemContainer.y = 45;
		addChild( assetsItemContainer );
		
		// Algorithm select
		var algorithmSelectContainer = new UIContentHolder( StorageOrder.VerticalLine, -1, -1, 0, 15);
		algorithmSelectContainer.x = 287;
		algorithmSelectContainer.y = 16;
		
		var algorithmLabel = new SimpleLabel();
		algorithmLabel.text = "2. Algorithm select";
		algorithmLabel.size = 18;
		algorithmSelectContainer.addContent( algorithmLabel );
		
		var algorithmSelectionRadioGroup = new SimpleRadioButtonGroup();
		//algorithmSelectionRadioGroup.addRadioButton( "Next-fit decreasing height (NFDH)" );
		algorithmSelectionRadioGroup.addRadioButton( "First-fit decreasing height (FFDH)" );
		algorithmSelectContainer.addContent( algorithmSelectionRadioGroup );
		
		addChild(algorithmSelectContainer);
		
		// Size select
		
		var sizeSelectContainer = new UIContentHolder( StorageOrder.VerticalLine, -1, -1, 0, 15);
		sizeSelectContainer.x = 287;
		sizeSelectContainer.y = algorithmSelectContainer.y + algorithmSelectContainer.height + 30;
		
		var sizeLabel = new SimpleLabel();
		sizeLabel.text = "3. Size select";
		sizeLabel.size = 18;
		sizeSelectContainer.addContent( sizeLabel );
		
		var sizeSelectionRadioGroup = new SimpleRadioButtonGroup();
		sizeSelectionRadioGroup.addRadioButton( "1024x1024" );
		sizeSelectionRadioGroup.addRadioButton( "2048x2048" );
		sizeSelectionRadioGroup.addRadioButton( "4096x4096" );
		sizeSelectionRadioGroup.addRadioButton( "8192x8192" );
		//sizeSelectionRadioGroup.addRadioButton( "Custom" );
		sizeSelectionRadioGroup.selectedIndex = 0;
		sizeSelectContainer.addContent( sizeSelectionRadioGroup );
		
		addChild(sizeSelectContainer);
		
		var packButton = new SimpleButton();
		packButton.buttonWidth = 300;
		packButton.buttonHeight = 50;
		packButton.labelText = "Pack";
		packButton.x = 287;
		packButton.y = screenHeight - packButton.height - 8;
		addScreenListener(packButton, MouseEvent.CLICK, onPackButtonClick);
		addChild(packButton);
		
	}
	
	private function addTooltips():Void
	{
		
	}
	
	private function addListeners():Void
	{
		#if cpp
		Lib.current.stage.application.window.onDropFile.add(onFileDrop, false, 0);
		#end
	}
	
	// EVENT HANDLERS
	
	private function onPackButtonClick(e:Event):Void 
	{
		//if ( assetsItemContainer.items.length < 1 )
		//{
			//Dialogs.message( "Error", "Please add some assets first. Aborting process.", true );
			//return;
		//}
		
		var packer:IPacker = new SimplePacker();
		packer.pack( [], 1024, 1024 );
		
		//Dialogs.saveFile( "Title", "Message", "asd" );
		//Dialogs.folder("Select destination", "");
		
	}
	
	override function onInit():Void 
	{
		setupUI();
		addTooltips();
		addListeners();
	}
	
	override function onResize():Void 
	{
		
	}
	
	private function onFileDrop(filename:String):Void
	{
		
	}
	
}