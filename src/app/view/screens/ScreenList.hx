package app.view.screens;
import com.creativemage.screenManager.AScreenList;

/**
 * ...
 * @author Creative Magic
 */
class ScreenList extends AScreenList
{
	public static inline var MAIN_SCREEN:String = "mainScreen";
	public static inline var MINI_VIEW:String = "miniView";

	public function new() 
	{
		super();
		
		push( MAIN_SCREEN, MainScreen );
		push( MINI_VIEW, MiniView );
	}
	
}